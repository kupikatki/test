﻿-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE testt;

--
-- Создать таблицу `groups`
--
CREATE TABLE groups (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 12,
AVG_ROW_LENGTH = 3276,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `name` для объекта типа таблица `groups`
--
ALTER TABLE groups
ADD UNIQUE INDEX name (name);

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE testt;

--
-- Создать таблицу `pings`
--
CREATE TABLE pings (
  ip int(11) UNSIGNED DEFAULT NULL,
  ts timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  data text DEFAULT NULL
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;



-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE testt;

--
-- Создать таблицу `servers`
--
CREATE TABLE servers (
  ip int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  group_id int(11) DEFAULT NULL,
  PRIMARY KEY (ip)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3232235781,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_general_ci;