<?php
class db {

    public function __construct($hostname, $username, $password, $database) {
        $this->link = mysqli_connect($hostname, $username, $password);
        mysqli_select_db($this->link, $database);
    }

    public function query($sql){
        return mysqli_query($this->link, $sql);
    }

    public function get_rows($sql){
        $res = $this->query($sql);
        $rows = self::resToArray($res);
        return $rows;
    }

    public function get_row($sql){
        $rows = $this->get_rows($sql);
        return $rows[0];
    }

    public static function resToArray($res) {
        $ret = array();
        while ($row = mysqli_fetch_assoc($res)) {
            $ret[] = $row;
        }
        return $ret;
    }

    public function escape($str){
        return mysqli_escape_string($this->link, $str);
    }
}