<?php
ini_set('default_charset', 'utf-8');
// пропишем рут, а то вдруг будем запускать что то из кли, а там нет относительных путей
define('ROOT', dirname(__FILE__) . "/..");
include_once(ROOT . "/app/config.php");

spl_autoload_register('loader');
function loader($className){
    include ROOT."/app/model/".$className.'.php';
}
