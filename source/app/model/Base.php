<?php
class Base {

    public $db;

    public function __construct($id = false){
        require_once(ROOT . '/system/db.php');
        $this->db = new db(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
    }

} 