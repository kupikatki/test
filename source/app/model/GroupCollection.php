<?php
class GroupCollection extends Base Implements IteratorAggregate, Countable {

    public function __construct($id = false){
        parent::__construct();
        $this->items = array();
    }

    /**
     * (non-PHPdoc)
     * @see IteratorAggregate::getIterator()
     */
    public function getIterator() {
        return new ArrayIterator($this->items);
    }

    /**
     * @return int
     */
    public function count() {
        return count($this->items);
    }
    
    public function init(){
        $rows = $this->db->get_rows("SELECT * FROM groups");
        foreach($rows as $row) {
            $this->items[$row['id']] = new Group($row['id']);
        }
    }
    
} 