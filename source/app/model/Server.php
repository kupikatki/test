<?php
class Server extends Base {

    private $pingHistory = false;

    public function __construct($ipInt = false, $ip = false){
        parent::__construct();
        if($ipInt){
            $this->data = $this->db->get_row("SELECT s.*, INET_NTOA(s.ip) ipFormat from servers s WHERE s.ip = {$ipInt}");
        }
        if($ip){
            $this->data = $this->db->get_row("SELECT s.*, INET_NTOA(s.ip) ipFormat from servers s WHERE s.ip = INET_ATON('{$ip}')");
        }
    }

    public function getIpInt(){
        return $this->data['ip'];
    }

    public function getIp(){
        return $this->data['ipFormat'];
    }

    public function ping(){
        exec("ping -n 1 {$this->getIp()}", $output, $status);
        // если status - 0 то сервер работает, поэтому чтобы небыло путаницы инвертируем
        $status = !$status;
        $this->db->query("INSERT INTO pings (ip,data) VALUES ({$this->getIpInt()}, {$status})");
        return $status;
    }

    public function getPingHistory(){
        if(!$this->pingHistory) {
            $this->pingHistory = $this->db->get_rows("SELECT p.ts, p.data from pings p WHERE p.ip = {$this->getIpInt()} ORDER BY p.ts DESC");
        }
        return $this->pingHistory;
    }

    public function save($ip, $group_id){
        // проверим на валидность, если валидно запишем, если нет ниче не делаем или выплюнем ошибку, но не сегодня
        if($group_id > 0 && $this->checkIp($ip)) {
            $this->db->query("INSERT INTO servers (ip, group_id) VALUES (INET_ATON('{$ip}'), {$group_id})");
        }
        return $this;
    }

    private function checkIp($ip){
        $ip = strtoupper($ip);
        if ($ip == '255.255.255.255'||$ip == '0xff.0xff.0xff.0xff'|| $ip == '0377.0377.0377.0377') {
            return true;
        }
        $tolong = ip2long($ip);
        if ($tolong == -1 || $tolong === FALSE)
            return false;
        else
            return true;
    }

} 