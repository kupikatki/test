<?php
class Group  extends Base{

    public function __construct($id = false){
        parent::__construct();
        if($id > 0){
            $this->data = $this->db->get_row("SELECT * FROM groups WHERE id = {$id}");
        }
    }

    public function getId(){
        return $this->data['id'];
    }

    public function getName(){
        return $this->data['name'];
    } 
    
    public function getServers(){
        $srvrs = $this->db->get_rows("SELECT s.ip FROM servers s where s.group_id = {$this->getId()}");
        $rows = array();
        foreach($srvrs as $s){
            $rows[] = new Server($s['ip']);
        }
        return $rows;
    }

    public function save($name){
        if($name != ''){
            $this->db->query("INSERT INTO groups (name) VALUES ('".$name."')");
        }
        return $this;
    }
    
}