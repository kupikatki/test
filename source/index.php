<?php
/**
 * Создать приложение для пинга серверов
    Сервера разбиты по группам
    Нужно предоставить пользователю интерфейс в виде html страницы
    с деревом групп и серверов, возможностью добавить группу, и  ip в конкретную группу
    выводом истории пингов и возможность пропинговать конкретный ip с выводом и сохранением результата.
    Возможность скачать csv файл с историей пингов по группе
    Результат (не используя composer, фреймворки и cms) в виде ссылки на github.
 */
include_once("/app/start.php");
$act = $_GET['act'];
switch($act){
    case 'addGroup':
        if(isset($_GET['groupName'])) {
            $group = new Group();
            $group->save($_GET['groupName']);
        }
        break;
    case 'addServer':
        if(isset($_GET['ip'])) {
            $server = new Server();
            $server->save($_GET['ip'], $_GET['groupId']);
        }
        break;
    case 'getCsv':
        if(isset($_GET['groupId'])){
            $isset = false;
            $f = fopen('php://memory', 'w');
            $group = new Group($_GET['groupId']);
            foreach($group->getServers() as $s){
                foreach($s->getPingHistory() as $p){
                    $isset = true;
                    fputcsv($f, array($s->getIp(),$p['ts'],$p['data']));
                }
            }
            fseek($f, 0);

            if($isset) {
                $filename = "export-group-{$_GET['groupId']}.csv";
                header('Content-Type: application/csv');
                header('Content-Disposition: attachment; filename="' . $filename . '";');
                fpassthru($f);
            } else {
                echo 'Нет пингов в этой группе';
            }
            die;
        }
        break;
    case 'ping':
        if(isset($_GET['ip'])){
            $server = new Server(false, $_GET['ip']);
            $server->ping();
        }
        break;
}

$groups = new GroupCollection();
$groups->init();

include_once(ROOT . "/app/view/index.html");